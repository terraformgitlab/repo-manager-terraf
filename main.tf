
resource "aws_vpc" "vpc_1" {
  cidr_block           = "10.123.0.0/16"
  enable_dns_hostnames = true #Find out para q se usa
  enable_dns_support   = true #Find out para q se usa

  tags = {
    Name = "dev"
  }
}