
terraform {
    backend "http" {      
    }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"                            # sino pongo la version el toma la latest version(la mas reciente)
    }
  }
}

provider "aws" {
  region = "us-east-1"
  #region = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_access_key
}